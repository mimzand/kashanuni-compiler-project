DEBUG = 0

import sys
### get input file address :
print("Please enter the address file : ")
address = input()

### open file
try:
    input_file = open(address,'r')
except (FileNotFoundError, IOError):
    print("ERROR :: Wrong file or file path!")
    sys.exit()

### get lines
input_lines = input_file.readlines()

### revision lines
for index in range(len(input_lines)):
    input_lines[index] = " ".join(input_lines[index].split()).replace("\n","").replace(" ,",",").replace(", ",",").replace(" :",":")

### first review for labet_table & db_table
label_table = {}
db_table = {}
for index in range(len(input_lines)):
    if ("db" in input_lines[index]) or ("DB" in input_lines[index]):
        if (input_lines[index].split()[1]=="db" or input_lines[index].split()[1]=="DB"):
            if(input_lines[index].split()[0] in db_table):
                print("WARNING :: duplicate name in db in line " + str(index) + "\n\t" + input_lines[index])
            db_table[input_lines[index].split()[0]] = ' '.join(input_lines[index].split()[2:])
        else:
            print("ERROR :: Syntax Error in line " + str(index) + ":\n\t" + input_lines[index])
    elif(":" in input_lines[index]):
        if(input_lines[index].split(":")[0] in label_table):
            print("WARNING :: duplicate name in labet in line " + str(index) + "\n\t" + input_lines[index])
        label_table[input_lines[index].split(":")[0]] = index

if(DEBUG):
        print('----------------')
        for tmp in db_table :
            print(tmp + ' = ' + str(db_table[tmp]))
        input()

### second review for execute
from Project1.function import functions
db_table["pc"] = 0
db_table["z"] = 0
db_table["c"] = 0
while True:
    if(db_table["pc"] == len(input_lines)):
        break
    if(len(input_lines)==0):
        print("ERROR :: input file is empty!")
        break
    if(len(input_lines[db_table["pc"]].split())==0):
        db_table["pc"] += 1
        continue
    if("HLT" in input_lines[db_table["pc"]].upper()):
        break
    if(":" in (input_lines[db_table["pc"]].split()[0])):
        instruction = " ".join(input_lines[db_table["pc"]].split()[1:])
    else:
        instruction = input_lines[db_table["pc"]][:]
    if (len(instruction.split())>2):
        print("ERROR :: Runtime error on line : " + str(db_table["pc"]) + "\n\t" + input_lines[db_table["pc"]])
        break
    elif (len(instruction.split()) == 1):
        if(instruction.split()[0].upper() in functions.functions_table):
            functions.functions_table[instruction.split()[0].upper()]
        else:
            print("ERROR :: operation not found in functions_table in line " + str(db_table["pc"]) + "\n\t" + input_lines[db_table["pc"]])
            break
    elif (len(instruction.split()) == 2):
        args = "".join(instruction.split()[1:])
        if(instruction.split()[0].upper() in ('JMP','JZ','JNZ','JE','JNE','JAE','JBE','JNAE','JNBE','JNA','JNB','JA','JB')):
            if(len(args.split(","))==1):
                if(args in label_table):
                    args = label_table[args]
                elif(args in db_table):
                    args = db_table[args]
                else:
                    print("ERROR :: label or var not found in line " + str(db_table["pc"]) + "\n\t" + input_lines[db_table["pc"]])
                    break
            if(instruction.split()[0].upper() in functions.functions_table):
                functions.functions_table[instruction.split()[0].upper()](db_table,args)
            else:
                print("ERROR :: operation not found in functions_table in line " + str(db_table["pc"]) + "\n\t" + input_lines[db_table["pc"]])
                break
        else:
            flag = False
            for var in args.split(","):
                if(var not in db_table):
                    print("ERROR :: var not found in line " + str(db_table["pc"]) + "\n\t" + input_lines[db_table["pc"]])
                    flag = True
                    break
            if(flag):
                break
            if(instruction.split()[0].upper() in functions.functions_table):
                functions.functions_table[instruction.split()[0].upper()](db_table,args)
            else:
                print("ERROR :: operation not found in functions_table in line " + str(db_table["pc"]) + "\n\t" + input_lines[db_table["pc"]])
                break
    db_table["pc"] += 1

    if(DEBUG):
        print(instruction)
        print('----------------')
        for tmp in db_table :
            print(tmp + ' = ' + str(db_table[tmp]))
        input()