def add(db, argList):
    arg=argList.split(",")
    if len(arg)==2:
        result=float(db[arg[0]]) + float(db[arg[1]])
    elif len(arg)==3:
        result= float(db[arg[1]]) + float(db[arg[2]])
    db[arg[0]]=result


def sub(db, argList):
    arg=argList.split(",")
    if len(arg)==2:
        result=float(db[arg[0]]) - float(db[arg[1]])
    elif len(arg)==3:
        result=float(db[arg[1]]) - float(db[arg[2]])
    db[arg[0]]=result


def mov(db, argList):
    arg=argList.split(",")
    db[arg[0]]=db[arg[1]]


def cmp(db, argList):
    arg=argList.split(",")
    if float(db[arg[0]])>float(db[arg[1]]):
        db["z"]=0
        db["c"]=0
    elif float(db[arg[0]])<float(db[arg[1]]):
        db["z"]=0
        db["c"]=1
    else:
        db["z"]=1
        db["c"]=0


def jmp(db, argList):
    db["pc"]=int(argList)-1


def inc(db, argList):
    db[argList] = float(db[argList]) + 1


def And(db, argList):
    arg=argList.split(",")
    db[arg[0]]= (float(db[arg[0]]) and float(db[arg[1]]))


def Or(db, argList):
    arg=argList.split(",")
    db[arg[0]]= (float(db[arg[0]]) or float(db[arg[1]]))


def ISZ(db, argList):
    db[argList]= float(db[argList]) + 1
    if float(db[argList])==0:
        db["z"]=1
        db["pc"]= int(db["pc"]) + 1
    else:
        db["z"]=0


def prt(db, argList):
    print(str(db[argList]))


def inp(db, argList):
    db[argList] = input()


def jz(db, argList):
    if(db["z"]==1): db["pc"]=int(argList)-1


def jnz(db, argList):
    if(db["z"]==0): db["pc"]=int(argList)-1


def jae(db, argList):
    if(db["c"]==0): db["pc"]=int(argList)-1


def jbe(db, argList):
    if(db["c"]==1 or db["z"]==1): db["pc"]=int(argList)-1


def ja(db, argList):
    if(db["c"]==0 and db["z"]==0): db["pc"]=int(argList)-1


def jb(db, argList):
    if(db["c"]==1): db["pc"]=int(argList)-1


def neg(db, argList):
    db[argList] = -float(db[argList])


def mul(db, argList):
    arg=argList.split(",")
    if len(arg)==2:
        result=float(db[arg[0]]) * float(db[arg[1]])
    elif len(arg)==3:
        result= float(db[arg[1]]) * float(db[arg[2]])
    db[arg[0]]=result


def div(db, argList):
    arg=argList.split(",")
    if len(arg)==2:
        result=float(db[arg[0]]) / float(db[arg[1]])
    elif len(arg)==3:
        result= float(db[arg[1]]) / float(db[arg[2]])
    db[arg[0]]=result