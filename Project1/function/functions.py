functions_table = {}

from Project1.function import operations

functions_table["ADD"] = operations.add
functions_table["SUB"] = operations.sub
functions_table["MOV"] = operations.mov
functions_table["CMP"] = operations.cmp
functions_table["JMP"] = operations.jmp
functions_table["INC"] = operations.inc
functions_table["AND"] = operations.And
functions_table["OR"] = operations.Or
functions_table["ISZ"] = operations.ISZ
functions_table["INP"] = operations.inp
functions_table["PRINT"] = operations.prt
functions_table["JZ"] = operations.jz
functions_table["JNZ"] = operations.jnz
functions_table["JE"] = operations.jz
functions_table["JNE"] = operations.jnz
functions_table["JAE"] = operations.jae
functions_table["JBE"] = operations.jbe
functions_table["JA"] = operations.ja
functions_table["JB"] = operations.jb
functions_table["JNAE"] = operations.jb
functions_table["JNBE"] = operations.ja
functions_table["JNA"] = operations.jbe
functions_table["JNB"] = operations.jae
functions_table["NEG"] = operations.neg
functions_table["MUL"] = operations.mul
functions_table["DIV"] = operations.div