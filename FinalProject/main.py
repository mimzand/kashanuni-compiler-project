reserved = (
    'STRING', 'DOUBLE', 'INPUT', 'PRINT', 'ELSE', 'WHILE'
)

tokens = reserved + (
    'ID',
    'ASSIGN', 'NUMBER', 'TEXT',
    'LPAR', 'RPAR',
    'NLINE', 'RBRACKET',
    'EQUAL', 'UNEQUAL', 'LARGER', 'LESS', 'LARGER_EQUAL', 'LESS_EQUAL',
    'PLUS','MINUS','TIMES','DIVIDE'
)

t_ASSIGN = r':='


def t_NUMBER(t):
    r'\d+(\.\d+)?'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t


def t_TEXT(t):
    r'\'.*\''
    t.value = t.value[1:-1]
    return t


t_LPAR          = r'\('
t_RPAR          = r'\)'
t_RBRACKET      = r'\}'
t_EQUAL         = r'='
t_UNEQUAL       = r'<>'
t_LARGER        = r'>'
t_LESS          = r'<'
t_LARGER_EQUAL  = r'>='
t_LESS_EQUAL    = r'<='
t_PLUS          = r'\+'
t_MINUS         = r'-'
t_TIMES         = r'\*'
t_DIVIDE        = r'/'
t_ignore        = " \t"


def t_NLINE(t):
    r'\n'
    t.lexer.lineno += 1
    return t


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

reserved_map = {}
for r in reserved:
    reserved_map[r.lower()] = r


def t_ID(t):
    r'[A-Za-z_]+'
    t.type = reserved_map.get(t.value,"ID")
    return t

# Build the lexer
import ply.lex as lex
lexer = lex.lex()

precedence = (
    ('left','PLUS','MINUS'),
    ('left','TIMES','DIVIDE'),
    ('right','UMINUS'),
)

id_table = {}
tmpID = 0;
lID = 0

def gettmp():
    global tmpID
    tmpID += 1
    return 'var'+str(tmpID-1)


def getlabel():
    global lID
    lID += 1
    return 'label'+str(lID-1)


def p_Start(t):
    'start : S'
    out = ''
    out += t[1][0]
    out += 'hlt' + '\n'
    for x in id_table:
        if(id_table[x][0] == ''): tmp = 0
        else: tmp = id_table[x][0]
        out += str(id_table[x][1]) + ' db ' + str(tmp) + '\n'
    try:
        output_file = open('output.asm','w')
    except (FileNotFoundError, IOError):
        print('ERROR in open output file')
    output_file.write(out)
    output_file.close()
    print(out)


def p_S(t):
    '''S : D NLINE S
         | D
         | I NLINE S
         | I
         | P NLINE S
         | P
         | W NLINE S
         | W
         |
    '''
    t[0] = {}
    if(len(t) == 4):
        if(type(t[1])== dict): t[0][0] = t[1][0] + t[3][0]
        else: t[0][0] = t[3][0]
    elif(len(t) == 2):
        if(type(t[1]) == dict): t[0][0] = t[1][0]
    else:
        t[0][0] = ''


def p_D(t):
    '''D : STRING ID
         | DOUBLE ID
    '''
    id_table[t[2]] = {}
    id_table[t[2]][0] = ''
    id_table[t[2]][1] = gettmp()


def p_I_1(t):
    'I : ID ASSIGN E'
    t[0] = {}
    if(t[1] in id_table):
        t[0][0] = t[3][0] + 'mov ' + id_table[t[1]][1] + ',' + t[3][1] + '\n'
    else: print('ERROR :: ' + t[1] + ' not defined')
	

def p_I_2(t):
    'I : ID ASSIGN TEXT'
    t[0] = {}
    if(t[1] in id_table):
        tmp = gettmp()
        id_table[tmp] = {}
        id_table[tmp][0] = t[3]
        id_table[tmp][1] = tmp
        t[0][0] = 'mov ' + id_table[t[1]][1] + ',' + tmp + '\n'
    else:
        print('ERROR :: ' + t[1] + ' not defined')


def p_I_3(t):
    'I : ID ASSIGN INPUT LPAR RPAR'
    t[0] = {}
    id_table[t[1]][0] = ''
    id_table[t[1]][1] = gettmp()
    t[0][0] = 'inp ' + id_table[t[1]][1] + '\n'


def p_P_1(t):
    'P : PRINT LPAR ID RPAR'
    t[0] = {}
    t[0][0] = 'print ' + id_table[t[3]][1] + '\n'


def p_P_2(t):
    'P : PRINT LPAR TEXT RPAR'
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = t[3]
    id_table[tmp][1] = tmp
    t[0][0] = 'print ' + tmp + '\n'


def p_P_3(t):
    'P : PRINT LPAR E RPAR'
    t[0] = {}
    t[0][0] = t[3][0] + 'print ' + t[3][1] + '\n'


def p_W(t):
    '''W : WHILE E LESS E NLINE S RBRACKET
        | WHILE E LESS E NLINE S RBRACKET NLINE ELSE NLINE S RBRACKET
        | WHILE E LARGER E NLINE S RBRACKET
        | WHILE E LARGER E NLINE S RBRACKET NLINE ELSE NLINE S RBRACKET
        | WHILE E LESS_EQUAL E NLINE S RBRACKET
        | WHILE E LESS_EQUAL E NLINE S RBRACKET NLINE ELSE NLINE S RBRACKET
        | WHILE E LARGER_EQUAL E NLINE S RBRACKET
        | WHILE E LARGER_EQUAL E NLINE S RBRACKET NLINE ELSE NLINE S RBRACKET
        | WHILE E UNEQUAL E NLINE S RBRACKET
        | WHILE E UNEQUAL E NLINE S RBRACKET NLINE ELSE NLINE S RBRACKET
        | WHILE E EQUAL E NLINE S RBRACKET
        | WHILE E EQUAL E NLINE S RBRACKET NLINE ELSE NLINE S RBRACKET
    '''
    l1 = getlabel()
    l2 = getlabel()
    l3 = getlabel()
    a = t[2][1]
    b = t[4][1]
    operator = t[3]
    code = ''

    code += 'cmp ' + a + ',' + b + '\n'

    if(operator == '='):    code += 'jne ' + l2 + '\n'
    elif(operator == '<>'): code += 'je '  + l2 + '\n'
    elif(operator == '<='): code += 'ja '  + l2 + '\n'
    elif(operator == '>='): code += 'jb '  + l2 + '\n'
    elif(operator == '>'):  code += 'jbe ' + l2 + '\n'
    elif(operator == '<'):  code += 'jae ' + l2 + '\n'

    code += l1 + ': '
    code += 'cmp ' + a + ',' + b + '\n'

    if(operator == '='):    code += 'jne ' + l3 + '\n'
    elif(operator == '<>'): code += 'je '  + l3 + '\n'
    elif(operator == '<='): code += 'ja '  + l3 + '\n'
    elif(operator == '>='): code += 'jb '  + l3 + '\n'
    elif(operator == '>'):  code += 'jbe ' + l3 + '\n'
    elif(operator == '<'):  code += 'jae ' + l3 + '\n'

    code += t[6][0]
    code += 'jmp ' + l1 + '\n'
    code += l2 + ': '
    if(len(t) == 13): code += t[11][0] + l3+ ': '

    t[0] = {}
    t[0][0] = code


def p_E(t):
    '''E : expression'''
    t[0] = {}
    t[0][0] = t[1][0]
    t[0][1] = t[1][1]


def p_expression_binop(t):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression'''
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = ''
    id_table[tmp][1] = tmp
    t[0][1] = tmp
    if t[2] == '+'  : t[0][0] = t[1][0] + t[3][0] + 'add ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '-': t[0][0] = t[1][0] + t[3][0] + 'sub ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '*': t[0][0] = t[1][0] + t[3][0] + 'mul ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'
    elif t[2] == '/': t[0][0] = t[1][0] + t[3][0] + 'div ' + tmp + ',' + t[1][1] + ',' + t[3][1] + '\n'


def p_expression_uminus(t):
    'expression : MINUS expression %prec UMINUS'
    t[0] = {}
    t[0][0] = t[2][0] + 'neg ' + t[2][1] + '\n'
    t[0][1] = t[2][1]


def p_expression_group(t):
    'expression : LPAR expression RPAR'
    t[0] = {}
    t[0][0] = t[2][0]
    t[0][1] = t[2][1]


def p_expression_number(t):
    'expression : NUMBER'
    t[0] = {}
    tmp = gettmp()
    id_table[tmp] = {}
    id_table[tmp][0] = float(t[1])
    id_table[tmp][1] = tmp
    t[0][1] = tmp
    t[0][0] = ''


def p_expression_name(t):
    'expression : ID'
    t[0] = {}
    t[0][0] = ''
    t[0][1] = ''
    if(t[1] in id_table):
        t[0][1] = id_table[t[1]][1]
    else: print('ERROR : ' + t[1] + ' not defined')


def p_error(t):
    print("Syntax error at '%s'" % t.value)


import ply.yacc as yacc
parser = yacc.yacc()

print("Please enter the address of input file : (default:input.txt)")
address = input()
if(address==''):address = 'input.txt'
try:
    input_file = open(address,'r')
except (FileNotFoundError, IOError):
    print('ERROR in open input file')

input_lines = input_file.readlines()
input_file.close()
input_str = ''
for line in input_lines:
    input_str += line

parser.parse(input_str)